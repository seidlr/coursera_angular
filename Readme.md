# conFusion-Angular
Start by running
<pre>npm install</pre><br/>
<pre>bower install</pre>
To emulate the backend you can use json-server. 
<pre>npm install json-server -g</pre>
You might need to to use <pre>sudo</pre> on ios.  
Create a folder json-server and put the db.json file in it. In the directory run
<pre>json-server --watch db.json</pre>
Now you can access the different resources.  
To run a local webserver you might use a node.js http-server.
<pre>npm install http-server</pre>
After installation <pre>cd</pre> into the app folder and run
<pre>http-server -o</pre>